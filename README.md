# Deploy a Kubernetes Cluster using Ansible

## Prepare Nodes

It's need to be generate a key on Master node and copy keys to the worker nodes. We have one Master and two Worker nodes.
Here 192.168.224.131 is Master and rest of two are Workers.

> ssh-keygen

> ssh-copy-id 192.168.224.129

> ssh-copy-id 192.168.224.130

> ssh-copy-id 192.168.224.131

## Install Ansible

Refreshing system’s package index and nstall the Ansible

> sudo apt update

> sudo apt install ansible

Setting up a new directory from which we we run our playbooks

> mkdir kubernetes

> cd kubernetes

Creating a hosts file to tell Ansible how to communicate with the Kubernetes master and worker nodes.

> nano hosts

It's ready to test Ansible

> ansible -i hosts all -m ping

[![Screenshot-2022-02-20-162716.png](https://i.postimg.cc/3x0LRYHG/Screenshot-2022-02-20-162716.png)](https://postimg.cc/4YZv2Cy4)

## Creating a Kubernetes cluster with Ansible Playbook

Setting up the Kubernetes cluster is to create a new user on each node.

> nano users.yml

> ansible-playbook -i hosts users.yml

[![Screenshot-2022-02-21-134124.png](https://i.postimg.cc/rFYgtLvq/Screenshot-2022-02-21-134124.png)](https://postimg.cc/w7LDSnqr)

Installing Kubernetes on each node.

> nano install-k8s.yml

> ansible-playbook -i hosts install-k8s.yml

[![Screenshot-2022-02-21-133941.png](https://i.postimg.cc/rwJbHHHb/Screenshot-2022-02-21-133941.png)](https://postimg.cc/jDDZwMtQ)

Creating the cluster on the master node.

> nano master.yml

> ansible-playbook -i hosts master.yml

[![Screenshot-2022-02-20-162623.png](https://i.postimg.cc/0Q1NHLmm/Screenshot-2022-02-20-162623.png)](https://postimg.cc/sBKyMHTD)

Joining the worker nodes to the cluster.

> nano join-workers.yml

> ansible-playbook -i hosts join-workers.yml

[![Screenshot-2022-02-20-162526.png](https://i.postimg.cc/gcMMyd3x/Screenshot-2022-02-20-162526.png)](https://postimg.cc/yWZXBMQK)

 Checking the status of the cluster nodes in master node.

 > kubectl get nodes

[![Screenshot-2022-02-20-162437.png](https://i.postimg.cc/XNDZcDqj/Screenshot-2022-02-20-162437.png)](https://postimg.cc/bdkNqg1W)

 It may take a short while for all the nodes to transition to the Ready status.

