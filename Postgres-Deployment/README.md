# Deploy PostgreSQL on Kubernetes

## Create and Apply ConfigMap

Creating a ConfigMap YAML file

> nano postgres-configmap.yaml


> kubectl apply -f postgres-configmap.yaml

The system confirms the successful creation of the configuration file.

## Create and Apply Persistent Storage Volume and Persistent Volume Claim

Creating a YAML file for storage configuration.

> nano postgres-storage.yaml


> kubectl apply -f postgres-storage.yaml

The system confirms the successful creation of both PV and PVC.

Checking that the PVC is connected to the PV

> kubectl get pvc


## Create and Apply PostgreSQL Deployment

Creating a deployment YAML

> nano postgres-deployment.yaml


> kubectl apply -f postgres-deployment.yaml

The system confirms the successful creation of the deployment.

## Create and Apply PostgreSQL Service

> nano postgres-service.yaml


> kubectl apply -f postgres-service.yaml


The system confirms the successful creation of the service.

[![Screenshot-2022-02-20-223435.png](https://i.postimg.cc/Pxzd4tHp/Screenshot-2022-02-20-223435.png)](https://postimg.cc/hzvF4Rfc)

## Listing all resources on the system

> kubectl get all


The pod and the deployment show the 1/1 ready status. The desired number of replica sets reflect what is configured in the deployment YAML file.

[![Screenshot-2022-02-20-223500.png](https://i.postimg.cc/yxT1j7Fp/Screenshot-2022-02-20-223500.png)](https://postimg.cc/dZLctPNG)

Connecting to PostgreSQL

> kubectl exec -it [pod-name] --  psql -h localhost -U admin --password -p [port] postgresdb

The system asks for the password. Type in the password defined in Step 1 and press Enter. The psql command prompt appears. The database is now ready to receive user input.

[![Screenshot-2022-02-20-222943.png](https://i.postimg.cc/sgBDR4Vc/Screenshot-2022-02-20-222943.png)](https://postimg.cc/nXJt479j)

Getting details of pod

> kubectl describe pods

[![Screenshot-2022-02-20-223223.png](https://i.postimg.cc/kMBjKTy9/Screenshot-2022-02-20-223223.png)](https://postimg.cc/dDcmKmXS)

For details, Check K8S cheatsheet https://kubernetes.io/docs/reference/kubectl/cheatsheet/
